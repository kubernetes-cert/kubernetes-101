# K8s Concepts 2

{+ Goal of this module is to understand following Concepts +}

- [X] [Pods](./pods/)
- [X] [ReplicationController & ReplicaSet](./replication-controller-set/)
- [X] [Deployment](./deployments/)

