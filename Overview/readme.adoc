= `Kubernetes` Architecture Overview

Must Read -
link:about-kubernetes.md[#*About Kubernetes*#]

'''
*Nodes*

A node is a machine, physical or virtual on which Kubernetes is installed. A node is a worker machine and that is where containers will be launched by Kubernetes.

[NOTE]
It was also known as minions in the past. So you might hear these terms used interchangeably.

*Cluster*

A Cluster is a set of nodes grouped together.

*Master*


The Master is another node with Kubernetes installed in it and is configured as a Master. The master watches over the nodes in the cluster and is responsible for the actual orchestration of nodes.

When we install Kubernetes on a system, we're actually installing the following components.

* An API server
* Controllers
* Schedulers
* kubelet service
* Container Runtime
* etcd service


#*The API server*# acts as the front end for Kubernetes. The users, management devices, command line interfaces, all talk to the API server to interact with Kubernetes cluster.

*#The controllers#* are the brain behind orchestration. They are responsible for noticing and responding when nodes, containers or end points goes down. The controllers make decisions to bring up new containers in such cases.

*#The Scheduler#* is responsible for distributing work or containers across multiple nodes. It looks for newly created containers and assigns them to nodes.

*#Kubelet#* is the agent that runs on each node in the cluster. The agent is responsible for making sure that the containers are running on the nodes as expected.

*#The Container Runtime#* is the underlying software that is used to run containers.

*#The etcd key store#* is a distributed reliable key value store used by Kubernetes to store all data used to manage the cluster.


_**Some `kubectl` commands - **_

`kubectl run hello-minikube` - deploy app on cluster

`kubectl cluster-info` -  to get cluster information.

`kubectl get nodes` - go list nodes in a cluster.

*Kubernetes Architecture*

.Kubernetes Architecture from https://kubernetes.io/
image::https://d33wubrfki0l68.cloudfront.net/2475489eaf20163ec0f54ddc1d92aa8d4c87c96b/e7c81/images/docs/components-of-kubernetes.svg[Kubernetes Architecture from https://kubernetes.io/]


.Simpler version of k8s architecture.
image::https://gitlab.com/hegdevenky/static-files/-/raw/master/devops/k8s-arch.png[]

image::https://gitlab.com/hegdevenky/static-files/-/raw/master/devops/kubernetes/k8s-overview.jpg[]




